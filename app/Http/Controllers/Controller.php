<?php

namespace App\Http\Controllers;
 
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function index(){
    	return view('welcome');

    }



}



php artisan make:migration <--filename-->

php artisan make:migration create_categories_table --create=categories

php artisan make:migration create_todos_table --create=todos